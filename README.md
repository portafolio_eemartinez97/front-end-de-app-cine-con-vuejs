# Frontend proyecto cine (VueJS)

En este proyecto se desarolló una aplicación que simula la reserva de entradas 
de cine (asientos), en este caso esta parte corresponde al front-end de la 
aplicación la cual se trabajó con VueJs

## Demo de la aplicación
[Demo de la APP](http://vue-cine-app.eemartinez97.com/#/)

**Email:** test@email.com

**Password:** 123456


## Temas que destacan dentro del proyecto

* Configuración de Webpack
* Types
* Namespaces
* Configuración de global store
* i18n
* Vuex
* State
* Acciones
* Getters
* Mutaciones
* Navegación y Router
* Autenticación con JWT
* Entre Otros